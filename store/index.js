export const state = () => ({
  userInfo: null,
  counter: 0,
  title: '애니볼' // 메인타이틀
})

export const mutations = {
  increment (state) {
    state.counter++
  },
  setUser (state, obj) {
    if (!obj) {
      return (state.userInfo = null)
    }

    state.userInfo = {}

    Object.entries(obj).forEach(([k, v]) => {
      if (typeof v === 'string') {
        state.userInfo[k] = v
      }
    })
  },
  setTitle (state, payload) {
    state.title = payload
  }
}
