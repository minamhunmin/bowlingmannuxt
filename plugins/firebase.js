import Vue from 'vue'
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/database'

const config = {
  apiKey: 'AIzaSyAomEIZI9AAxbPYURKx5VsOXNaW-ZER6Kc',
  authDomain: 'bowlingmanmini.firebaseapp.com',
  databaseURL: 'https://bowlingmanmini.firebaseio.com',
  projectId: 'bowlingmanmini'
}

export default function ({ store }) {
  if (!firebase.apps.length) {
    firebase.initializeApp(config)
  }

  Vue.prototype.$db = firebase.firestore()
  Vue.prototype.$auth = firebase.auth()
  Vue.prototype.$authProviders = {
    Google: firebase.auth.GoogleAuthProvider.PROVIDER_ID
  }

  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
      console.log(user)
    } else {
      console.log('ddd')
    }
    store.commit('setUser', user)
  })
  // Vue.use(Vuetify, {
  //   iconfont: 'mdi'
  // })
}
