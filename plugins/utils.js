import Vue from 'vue'

Vue.prototype.$utils = {
  findObject (list, key, value) {
    let result = ''
    list.forEach((element) => {
      if (element[key] === value) {
        result = element
      }
    })

    return result
  }

}
